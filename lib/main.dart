import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:lecture_practice_15_redux_navigation/navigation_view_model.dart';
import 'package:lecture_practice_15_redux_navigation/routes/route_helper.dart';
import 'package:lecture_practice_15_redux_navigation/store/app_state.dart';
import 'package:redux/redux.dart';

void main() {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Store store = Store<AppState>(
      AppState.getAppReducer,
      initialState: AppState.initial(),
      middleware: [
        NavigationMiddleware<AppState>(),
      ],
    );
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        navigatorKey: NavigatorHolder.navigatorKey,
        onGenerateRoute: (RouteSettings settings) =>
            RouteHelper.instance.onGenerateRoute(settings),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, NavigationViewModel>(
      converter: NavigationViewModel.fromStore,
      builder: (BuildContext context, NavigationViewModel vm) => Scaffold(
        appBar: AppBar(
          title: Text('Redux navigation'),
        ),
        drawer: Drawer(
          child: Container(
            height: 500.0,
            child: Column(
              children: [
                Spacer(),
                ElevatedButton(
                  onPressed: () {
                    vm.goToFirstPage();
                  },
                  child: Text(
                    'to 1 page',
                    style: TextStyle(fontSize: 30),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    vm.goToSecondPage();
                  },
                  child: Text(
                    'to 2 page',
                    style: TextStyle(fontSize: 30),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    vm.goToThirdPage();
                  },
                  child: Text(
                    'to 3 page',
                    style: TextStyle(fontSize: 30),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    vm.goToFourthPage();
                  },
                  child: Text(
                    'to 4 page',
                    style: TextStyle(fontSize: 30),
                  ),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
        body: Center(
          child: ElevatedButton(
            child: Text('To first page'),
            onPressed: () {
              vm.goToFirstPage();
            },
          ),
        )
      ),
    );
  }
}
