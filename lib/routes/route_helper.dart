

import 'package:flutter/material.dart';
import 'package:lecture_practice_15_redux_navigation/fourth_page.dart';
import 'package:lecture_practice_15_redux_navigation/routes/routes.dart';
import 'package:lecture_practice_15_redux_navigation/third_page.dart';

import '../first_page.dart';
import '../main.dart';
import '../second_page.dart';


class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';
  RouteHelper._privateConstructor();
  static final RouteHelper _instance = RouteHelper._privateConstructor();
  static RouteHelper get instance => _instance;
  // endregion
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case firstPageRoute:
        return _defaultRoute(
          settings: settings,
          page: FirstPage(),
        );
      case secondPageRoute:
        return _defaultRoute(
          settings: settings,
          page: SecondPage(),
        );
      case thirdPageRoute:
        return _defaultRoute(
          settings: settings,
          page: ThirdPage(),
        );
      case fourthPageRoute:
        return _defaultRoute(
          settings: settings,
          page: FourthPage(),
        );
      case mainPageRoute:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
      default:
        return _defaultRoute(
          settings: settings,
          page: MyHomePage(),
        );
    }
  }
  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}