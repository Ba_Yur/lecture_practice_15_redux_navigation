
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:lecture_practice_15_redux_navigation/routes/routes.dart';
import 'package:redux/redux.dart';

import 'app_state.dart';

class NavigationSelector {
  static void Function() navigateToFirstPage(Store<AppState> store) {
    return () => store.dispatch(NavigateToAction.push(firstPageRoute));
  }

  static void Function() navigateToSecondPage(Store<AppState> store) {
    return () => store.dispatch(NavigateToAction.push(secondPageRoute));
  }

  static void Function() navigateToThirdPage(Store<AppState> store) {
    return () => store.dispatch(NavigateToAction.push(thirdPageRoute));
  }

  static void Function() navigateToFourthPage(Store<AppState> store) {
    return () => store.dispatch(NavigateToAction.push(fourthPageRoute));
  }

  static void Function() navigateToMainPage(Store<AppState> store) {
    return () => store.dispatch(NavigateToAction.push(mainPageRoute));
  }
}

