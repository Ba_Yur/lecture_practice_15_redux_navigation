import 'package:flutter/foundation.dart';

class AppState {
  // final CounterPageState counterPageState;
  // final PhotosState photosState;


  AppState(
    // @required this.counterPageState,
    // @required this.photosState,
  );

  factory AppState.initial() {
    return AppState(
      // counterPageState: CounterPageState.initial(),
      // photosState: PhotosState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      // counterPageState: state.counterPageState.reducer(action),
      // photosState: state.photosState.reducer(action),
    );
  }

}