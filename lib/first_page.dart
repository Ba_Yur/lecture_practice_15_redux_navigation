import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:lecture_practice_15_redux_navigation/navigation_view_model.dart';
import 'package:lecture_practice_15_redux_navigation/store/app_state.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, NavigationViewModel>(
      converter: NavigationViewModel.fromStore,
      builder: (BuildContext context, NavigationViewModel vm) {
        return Scaffold(
          appBar: AppBar(
            title: Text('1'),
          ),
          drawer: Drawer(
            child: ElevatedButton(
                onPressed: () {
                  vm.goToMainPage();
                },
                child: Text('To main page')),
          ),
          body: Center(
            child: Container(
              height: 300,
              child: Column(
                children: [
                  Text(
                    '1',
                    style: TextStyle(fontSize: 50),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
