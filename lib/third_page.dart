import 'package:flutter/material.dart';

class ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('3'),
      ),
      body: Center(
        child: Text(
          '3',
          style: TextStyle(fontSize: 50),
        ),
      ),
    );
  }
}
