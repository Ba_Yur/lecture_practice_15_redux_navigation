import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('2'),
      ),
      body: Center(
        child: Text(
          '2',
          style: TextStyle(fontSize: 50),
        ),
      ),
    );
  }
}
