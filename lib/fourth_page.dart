import 'package:flutter/material.dart';

class FourthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('4'),
      ),
      body: Center(
        child: Text(
          '4',
          style: TextStyle(fontSize: 50),
        ),
      ),
    );
  }
}
