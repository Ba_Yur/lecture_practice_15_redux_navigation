import 'package:flutter/material.dart';
import 'package:lecture_practice_15_redux_navigation/store/app_state.dart';
import 'package:lecture_practice_15_redux_navigation/store/navigation_selector.dart';
import 'package:redux/redux.dart';

class NavigationViewModel {
  final void Function() goToFirstPage;
  final void Function() goToSecondPage;
  final void Function() goToThirdPage;
  final void Function() goToFourthPage;
  final void Function() goToMainPage;

  NavigationViewModel({
    @required this.goToFirstPage,
    @required this.goToSecondPage,
    @required this.goToThirdPage,
    @required this.goToFourthPage,
    @required this.goToMainPage,
  });

  static NavigationViewModel fromStore(Store<AppState> store) {
    return NavigationViewModel(
      goToFirstPage: NavigationSelector.navigateToFirstPage(store),
      goToSecondPage: NavigationSelector.navigateToSecondPage(store),
      goToThirdPage: NavigationSelector.navigateToThirdPage(store),
      goToFourthPage: NavigationSelector.navigateToFourthPage(store),
      goToMainPage: NavigationSelector.navigateToMainPage(store),
    );
  }
}
